CREATE TABLE myuser (
        id INTEGER NOT NULL, 
        username VARCHAR(255) NOT NULL, 
        password VARCHAR(255), 
        PRIMARY KEY (id)
)

CREATE TABLE post (
        id INTEGER NOT NULL, 
        title VARCHAR(255) NOT NULL, 
        text TEXT, 
        publish_date DATETIME, 
        user_id INTEGER, 
        PRIMARY KEY (id), 
        FOREIGN KEY(user_id) REFERENCES myuser (id)
)

CREATE TABLE comment (
        id INTEGER NOT NULL, 
        name VARCHAR(255) NOT NULL, 
        text TEXT, 
        date DATETIME, 
        post_id INTEGER, 
        PRIMARY KEY (id), 
        FOREIGN KEY(post_id) REFERENCES post (id)
)

CREATE TABLE tag (
        id INTEGER NOT NULL, 
        title VARCHAR(255), 
        PRIMARY KEY (id), 
        UNIQUE (title)
)

CREATE TABLE post_tags (
        post_id INTEGER, 
        tag_id INTEGER, 
        FOREIGN KEY(post_id) REFERENCES post (id), 
        FOREIGN KEY(tag_id) REFERENCES tag (id)
)
